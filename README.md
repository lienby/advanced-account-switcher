# Releases 

# v1.2
+ Added tm0:npdrm/act.dat backup when saving accounts
+ Added tm0:psmdrm/act.dat backup when saving accounts

both of these can be disabled in the options menu.

VPK https://bitbucket.org/SilicaAndPina/advanced-account-switcher/downloads/Advanced_Account_Switcher-1.2.vpk  

# v1.1
+ Fixed a bug where sceRegMgrSetKeyStr wouldn't work.

VPK https://bitbucket.org/SilicaAndPina/advanced-account-switcher/downloads/Advanced_Account_Switcher-1.1.vpk  

# v1.0
When SimpleAccountSwitcher just isnt enough!

Features:

+ Save / Load accounts
+ Change the linked AID
+ Removed the linked account
+ Run the PSN Sign Up Application.

VPK https://bitbucket.org/SilicaAndPina/advanced-account-switcher/downloads/Advanced_Account_Switcher-1.0.vpk  